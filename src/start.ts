import app from './app'

app.get('/ping', (req: any, res: any) => {
  return res.send('ok!')
})

app.listen(8080, () =>
  console.log('listening on localhost:8080/ping')
)