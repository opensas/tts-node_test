# test project for running ts-node with ttypescript

For context see: https://github.com/cevek/ttypescript/issues/71#issuecomment-560035672

## Steps to reproduce

```shell
$ git clone git@gitlab.com:opensas/tts-node_test.git

$ cd tts-node_test\

$ npm install
```

Try with `ts-node` and `tsconfig-paths/register`

```shell
λ npm run ts-node

> tmp@1.0.0 ts-node C:\data\devel\apps\sgte-it\coordinacion\juridicos\tmp2\tts-node_test
> ts-node -r tsconfig-paths/register src/start.ts

listening on localhost:8080/ping
```

Try with `ts-node` and `ttypescript` (Error!)

```shell
λ npm run tts-node

> tmp@1.0.0 tts-node C:\data\devel\apps\sgte-it\coordinacion\juridicos\tmp2\tts-node_test
> ts-node -C ttypescript src/start.ts

C:\data\devel\apps\sgte-it\coordinacion\juridicos\tmp2\tts-node_test\src\app.ts:1
import express from 'express'
^^^^^^

SyntaxError: Cannot use import statement outside a module
    at Module._compile (internal/modules/cjs/loader.js:881:18)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:962:10)
    at Module.load (internal/modules/cjs/loader.js:798:32)
    at Function.Module._load (internal/modules/cjs/loader.js:711:12)
    at Module.require (internal/modules/cjs/loader.js:838:19)
    at require (internal/modules/cjs/helpers.js:74:18)
    at Object.<anonymous> (C:\data\devel\apps\sgte-it\coordinacion\juridicos\tmp2\tts-node_test\src\start.ts:1:1)
    at Module._compile (internal/modules/cjs/loader.js:945:30)
    at Module.m._compile (C:\data\devel\apps\sgte-it\coordinacion\juridicos\tmp2\tts-node_test\node_modules\ts-node\src\index.ts:536:23)
    at Module._extensions..js (internal/modules/cjs/loader.js:962:10)
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! tmp@1.0.0 tts-node: `ts-node -C ttypescript src/start.ts`
npm ERR! Exit status 1
npm ERR!
npm ERR! Failed at the tmp@1.0.0 tts-node script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     C:\Users\sas\AppData\Roaming\npm-cache\_logs\2019-12-01T07_36_41_041Z-debug.log
```

You may also just issue:

```shell
$ npx ts-node -r tsconfig-paths/register src/start.ts
```

and

```shell
$ npx ts-node -C ttypescript src/start.ts
```
